# Irrigation with raspberry pi

[[_TOC_]]

## Behavior

The automatic irrigation system must water and track the moisture of my indoor plant garden. The
plants are located in a single room but have different sizes and water needs. For this reason, the
system will require a main central hose with N smaller pipes ended with drippers that can be
regulated. The watering system can be activated manually (considering web or telegram bot
interfaces) or can be programmed with a cronjob.

As there is no water outlet in the space, the water will be extracted from a reservoir, likely a
large bucket that can be placed on an elevated place to help the water movement to the whole pot
area. It would be interesting to also be able to track the water level of the reservoir and access
this information via web or telegram bot.

To start the irrigation cycle, the Raspberry Pi will emit a radio signal (433 Mhz) to switch on a power outlet where the water pump will be connected. Once the wanted amount of water is pumped out, the Rpi will emit another signal to swith off he radio-controlled power outlet.

The system will need to read and show soil moisture in different (probably not all) pots to make
sure that the watering system is working properly. The interface to these readings can also be web
based or using a telegram bot.

Finally, the system could likely use a camera to remotely see the progress of the plants, although
this is an extra-feature that has no priority.

## Hardware schema

## Software implementation

## Component research


### Water Pump

Need to get water out of a container and into a hose, for which we need a pump.

AC vs DC? We are gonna need an AC water pump, so that we can connect it to the remotely controlled power
outlet.

Some AC powered pumps (generally used for aquariums and home DIY hydroponic systems) that I've
considered:

* AC: 8W - 600L/H (1.2m height): https://www.amazon.es/Homvik-Sumergible-Estanque-Circulaci%C3%B3n-Boquillas/dp/B07HFXNP1C
* AC: 45W - 2500L/H (2.5m height): https://www.amazon.es/dp/B07P7L3Z3T/

The Homvik 600L/H (8W) is quite perfect for this. It also has a 1.5m cable and an adjustable valve.
The size of this pump is 58x44x55 mm.

The Homvik 2500L/H is quite bigger, probably not necessary to get something as powerful as this. The
size of this is 105x65x95 mm.

A mid-way option is the Homvik 1500L/H (25W) which is 113x56x70 mm.

**Conclusion:**

Considering the help of gravity and the low amount of water needed for the whole irrigation cycle (3-4 Liters in total), the **Homvik 600L/H** is more than enough.

This pump has two different outlets, to which we'll need to connect the main hose: a 0.5 in (12 mm) and 0.3 in (8 mm)


### Hose/irrigation tubes

We are gonna need one to the main hose, that is directly connected to the pump. We need this one to
be 12mm or 8mm, and a minimum of 5/6 meters long.

https://www.amazon.es/gp/product/B01FU8ESJ0/

Out of this hose we can connect thinner outlet hoses that will end on regulated drippers. The kind
of hoses that we find for dripping systems are generally thinner (4mm).

We are also gonna need 3-way T-shaped connectors to create the water outlets to the plants:

These are for 4-7mm hoses:

* https://www.amazon.es/conectores-manguera-riego-sistema-goteo/dp/B07FNH1QL9 For 12mm hoses:
* https://www.manomano.es/p/te-de-union-12mm-5p-852877#/

Ideas for piping:

* https://www.fitgardening.com/drip-irrigation/index.html


### Irrigation drippers

Some options found:

* https://www.amazon.es/JVSISM-Micro-Boya-Invernadero-Pulverizador-Aspersores/dp/B07NNBJ1BJ
* https://www.amazon.es/Altear-Irrigaci%C3%B3n-Goteadores-Sprinklers-Regulable/dp/B08FLB9FL2/

Not sure about these, it would be great that the drippers don't spray the water around (as some pots
are quite small), but I can't find anything that can regulate a single stream of water, all of the
options spray it all around the dripper.


### About the Raspberri pi GPIO

Raspberry Pi has a row of General Purpose Input/Output that we will use to:

* Input the sensor readings
* Output the radio signal to start the irrigation cycle
* Input the radio signal (temporary, just for the development process) to capture the radio emitter
  IDs

https://www.raspberrypi.org/documentation/usage/gpio/

#### My Raspberry Pi B GPIO guide

![Raspberry Pi GPIO pinouts](http://raspi.tv/wp-content/uploads/2014/07/Raspberry-Pi-GPIO-pinouts-1024x703.png)

Image from: http://raspi.tv/wp-content/uploads/2014/07/Raspberry-Pi-GPIO-pinouts-1024x703.png

#### Introduction to GPIO programming

Some interesting links with guides and tutorials:

* https://tutorials-raspberrypi.com/raspberry-pi-gpio-explanation-for-beginners-programming-part-2/
* https://www.dummies.com/computers/raspberry-pi/raspberry-pi-gpio-ports/
* https://www.dummies.com/computers/raspberry-pi/how-to-control-gpio-pins-on-your-raspberry-pi/


### Water/moisture sensors

We need to connect multiple sensors.

One question is whether they can be connected in series and have one single data wire going to the
Rpi GPIO, at least certain sensors can do that as they have unique serial numbers. Here's an
example of this with temperature sensors:

http://www.reuk.co.uk/wordpress/raspberry-pi/connect-multiple-temperature-sensors-with-raspberry-pi/

Another example of multiple soil moisture sensors at once:
https://4.bp.blogspot.com/-fTqP7suYzwk/ViktwyvsH3I/AAAAAAAAA4s/65u56as2EHE/s1600/multiple%2Bsoil%2Bmoisture%2Bsensor.png

Problem with sensors: they can get deteriorated really quickly.

* https://www.switchdoc.com/2020/06/tutorial-capacitive-moisture-sensor-grove/

#### Resistive moisture sensors

The most common type of sensors found are resistive moisture sensors.
See: https://www.amazon.es/ARCELI-higr%C3%B3metro-detecci%C3%B3n-Humedad-Arduino/dp/B07CQT5RC8/

* They are more accurate
* But they deteriorate really fast

#### Capacitive moisture sensors

* Don't deteriorate as fast
* Not so accurate
* Need an analogical/digital converter module to use with Rpi

Some purchasable sensor sets:

* https://www.amazon.es/Aideepen-capacitivo-resistente-corrosi%C3%B3n-anal%C3%B3gico/dp/B08GCRZVSR/
* https://www.amazon.es/ZSWQ-M%C3%B3dulo-Humedad-detecci%C3%B3n-Arduino/dp/B08GC5KT4T/
* https://www.amazon.es/SANON-Capacitivo-Anal%C3%B3gico-Resistente-Corrosi%C3%B3n/dp/B08941N8CZ/

**PiPlanter** is an example of a nice project that uses these sensors. The website comes with lots
of really detailed explanations, so it's an important resource.

* https://www.esologic.com/piplanter-2/


#### ADC converters


```
RasPiO Analog Zero board uses the MCP3008 chip and can connect to any 40-pin Raspberry Pi, as
well as Raspberry Pi Zero and older 26-pin Raspberry Pi (with the aid of a Downgrade GPIO Ribbon
Cable)
```

Some really good information about MCP3008 vs ADS1015/ADS1115 in:

* https://www.digikey.com/en/maker/projects/raspberry-pi-analog-to-digital-converters/72388f5f1a0843418130f56c53a1276c


##### MCP3008 chip

* 8 channels and 10 bits
* A great option to read simple signals like temperature or light sensors
* Can use a hardware SPI bus or 4 GPIO pins + software SPI (this is more flexible and easier to
  setup)
* Wiring:
  * https://www.esologic.com/wp-content/uploads/2013/02/Capture.PNG
  * Connect to 1 Rpi 3.3v, 1 Rpi GND, 4 data pins (GPIO18, GPIO23, GPIO24, GPIO25).
* Library: Adafruit_Python_MCP3008 library:
  * https://github.com/adafruit/Adafruit_Python_MCP3008
  * This library can access all the ADC channels and their values: 8 channels


##### ADS1015/ADS1115 chips

* Easy to use with I2C communication bus
  * ADS1015: 12-bit 4-channel
  * ADS1115: 16-bit 4-channel
* More precission
* Wiring:
  * Need to enable I2C using raspi-config
  * Connect to one 3.3V, one GND, SCL (GPIO1) and SDA (GPIO0)
* Library: Adafruit ADS1x15 Python library:
  * https://github.com/adafruit/Adafruit_Python_ADS1x15
  * You can access the code channels, that will have high precision values (12 or 16 bit depending
    on the chip)

##### Conclusion:

We will use a MCP3008 chip on a breadboard, wired to the GPIO 3.3V and GND, and eight capacitive
moisture sensors, each one connected to 3.3V, GND and one data channel of the MCP3008 chip.

This will only use 1 3.3V, 1 GND and 4 GPIO data pins in the Rpi and we can read a total of 8
analogical sensors.



### Remote control switches 433Mhz

We will need two elements: a radio activated power outlet that receives on/off signals (with a
unique ID) and switches on/off the outlet. The pump (or whatever other domotic element that we
want to use in the future) will be powered by these sources and can be hence switched on and off
either by using the remote control or by using radio signals emitted by the Rpi.

For this, we also need a radio-emitting chip connected to the Raspberry, that we will use to switch
on/off the plugs by emitting radio signals on the 433Mhz frequency with the unique ID that the power
outlet needs. This unique ID can be read by testing the remote control and reading the signal with a
radio receiver chip.

433MHz radio activated switches with their remotes (Etekcity):

* https://www.amazon.es/dp/B016I3U9H6/
* https://www.amazon.es/Etekcity-Inal%C3%A1mbricos-Inteligentes-Electrodom%C3%A9sticos-Aprendizaje/dp/B016I3TZ58

433MHz radio emitterand receiver chips for Rpi:

* https://www.amazon.com/Willwin-Wireless-Transmitter-Receiver-Raspberry/dp/B076Q64MBQ/

Some Howtos:

* https://zenofall.com/simple-home-automation-using-raspberry-pi-with-433mhz-radio-chips-etekcity-remote-outlet-and-python/
* https://www.instructables.com/RF-433-MHZ-Raspberry-Pi/


#### rpi-rf module

[https://pypi.org/project/rpi-rf/](https://pypi.org/project/rpi-rf/)

```
Python module for sending and receiving 433/315MHz LPD/SRD signals with generic low-cost GPIO RF
modules on a Raspberry Pi.
```


### Camera

Pi NoIR Camera V2:

* https://www.raspberrypi.org/products/pi-noir-camera-v2/?resellerType=home
* (Comes with a blue filter) 30€


## Interesting links

* https://www.hackster.io/ben-eagan/raspberry-pi-automated-plant-watering-with-website-8af2dc
* https://www.randomgarage.com/2018/12/raspberry-pi-automated-irrigation-system.html
* Simple circuit (no details about the pump): https://www.hackster.io/sureshsarika/water-pump-controller-d86d17
* https://www.hackster.io/hemal-sebastian/automatic-water-pump-31215f
* With telegram bot! sick: https://www.hackster.io/zenofall/community-iot-garden-using-raspberry-pi-and-telegram-bot-ef4989
  * Telegram group: https://t.me/iotgarden Setup
  * https://zenofall.com/wp-content/uploads/2019/04/system_diag.png
* Really cool garden IoT project: https://www.instructables.com/Raspberry-Pi-Powered-IOT-Garden/


## Shopping list

* [x] Breadboard
* [x] Jumper cables
* [x] 433 Mhz radio transmitter and receiver
* [x] Remote switches (433 Mhz)
* [x] Pump
* [ ] Rpi Camera
* [ ] MCP3008 chip
* [x] 8 capacitive moisture sensors
* [ ] 1/2'' hose
* [ ] T-shaped hose connectors
* [ ] 1/4'' hose
* [ ] irrigation drippers with regulator



